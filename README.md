Obtain 21 using 1, 5, 6, and 7. You must use each of them exactly once. You can only use +, -, * and /. You can use parentheses freely.

Note: there are no tricks; the values above are meant as numbers, not as digits (e.g. using 15, 6 and 7 is not allowed).
