from __future__ import division
import random
import copy

def add():
    b,a = stack.pop(),stack.pop()
    stack.append(a+b)
def sub():
    b,a = stack.pop(),stack.pop()
    stack.append(a-b)
def mul():
    b,a = stack.pop(),stack.pop()
    stack.append(a*b)
def div():
    b,a = stack.pop(),stack.pop()
    stack.append(a/b)

digits = [1, 5, 6, 7]
target = 21.0

ops = [add, sub, mul, div]
while True:
    stack = []
    digs = copy.copy(digits)
    random.shuffle(digs)
    saved_ops = []
    while True:
        if len(stack) >= 2 and random.randint(0, 1):
            o = random.choice(ops)
            saved_ops.append(o)
            try:
                o()
            except:
                stack = []
                break
        elif len(digs):
            saved_ops.append(digs[-1])
            stack.append(digs.pop())
        else:
            break
    if len(stack) == 1 and abs(stack[0]-target)<1E-5:
        print saved_ops
        print "hooray!"
        break
